import React from 'react';
import logo from './GitLab_Logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter } from 'react-router-dom';

import Card from 'react-bootstrap/Card';

class App extends React.Component {

  state = {loading: true, token: '', username: '', name: '', avatar: ''}

  /**
   * Making an async start. First calling ayncLoad() and if the access token recieved, 
   * making a request of user info using the access token. Updating the state with the info from the request.
   * @ignore
   */
  componentDidMount() {
    this.asyncLoad().then(() => {
      if(this.state.token) {
        fetch('https://gitlab.com/api/v4/user', {  
          method: 'GET',
          headers: { 'Authorization': 'Bearer ' + this.state.token }
        }).then(response => response.json()).then(userInfo => {
          this.setState({loading: false, username: userInfo.username, name: userInfo.name, avatar: userInfo.avatar_url});
        });
      }
    });
  }

  /**
   * Making a request to get an access token. The request will redirect back to app with the access token inside the url.
   * Using the location to check if we recieved the token, if so updating the state.
   * @ignore
   */
  async asyncLoad() {

    const { location } = this.props;

    /**
     * Update the state and return if the access token exists in the url.
     * @ignore
     */
    if(location.hash) {
      const token = [...new URLSearchParams(location.hash.substring(1)).entries()][0][1];
      return this.setState({ token: token });
    }

    /**
     * Setting up the params for the request. Making sure the redirect uri is compatible with the environment.
     * @ignore
     */
       
    const params = new URLSearchParams({
      response_type: 'token',
      client_id: process.env.REACT_APP_APPLICATION_ID,
      redirect_uri: process.env.REACT_APP_REDIRECT_URI
    });

    /**
     * Making the request.
     * @ignore
     */
    window.location = `https://gitlab.com/oauth/authorize?${params.toString()}`;
  }

  render() {

    const { loading } = this.state;

    if(loading) {
      return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
          </header>
          <div className="App-card">
            <p className="App-loading">Loading...</p>
          </div>
        </div>
      )
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <div className="App-card">
          <Card className="Card">
            <Card.Img variant="top" src={this.state.avatar}></Card.Img>
            <Card.Body>
              <Card.Title>{this.state.name}</Card.Title>
              <Card.Text>@{this.state.username}</Card.Text>
            </Card.Body>
          </Card>
        </div>
      </div>
    );
  }
}

export default withRouter(App);
